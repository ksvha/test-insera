<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IdentitasController extends Controller
{
    public function index(){
        $data_id = \App\Identittas::all();
        return view('identitas.index',['data_id' => $data_id]);
    }

    public function create(Request $request)
    {
        // dd($request->all());
        $t=$request->input('tmpt_lahir');
        $tl=$request->input('tl');
        $ttl= "$t, $tl";
        
        \App\Identittas::create([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'status' => $request->status,
        'jk' => $request->jk,
        'ttl' => $ttl,
        'alamat' => $request->alamat,
        'agama' => $request->agama,
        'kewarganegaraan' => $request->kewarga,
        'pekerjaan' => $request->pekerjaan,
        'masa_berlaku' => $request->berlaku,
        ]);
        return redirect('/identitas')->with('sukses','Data Berhasil di Tambahkan');
    }

    public function edit($id)
    {
        $identitas = \App\Identittas::find($id);
        return view('identitas/edit',['identitas' => $identitas]);
        // dd($identitas);
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $identitas = \App\Identittas::find($id);
        $identitas->update($request->all());
        return redirect('/identitas')->with('sukses','Data Berhasil di Tambahkan');
    }

    public function delete($id)
    {
        // dd($id);
        \App\Identittas::destroy($id);
        return redirect('/identitas')->with('sukses','Data Berhasil di Hapus');
    }
}
