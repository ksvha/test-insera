<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identittas extends Model
{
    protected $table = 'identittas';
    protected $fillable = ['nik','nama','status','ttl','jk','alamat','pekerjaan','kewarganegaraan','masa_berlaku','agama'];
}
