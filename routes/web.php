<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/identitas','IdentitasController@index');
Route::post('/identitas/create','IdentitasController@create');
Route::get('/identitas/edit/{id}','IdentitasController@edit');
Route::post('/identitas/update/{id}','IdentitasController@update');
Route::get('/identitas/delete/{id}','IdentitasController@delete');
