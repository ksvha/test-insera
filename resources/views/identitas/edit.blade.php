<!doctype html>
<html>
  <head>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Form Identitas</title>
  </head>
<body>
        <div class="container">
        @if(session('sukses'))
            <div class="alert alert-success" role="alert">
            {{session('sukses')}}
            </div>
        @endif
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data
                </div>
                <div class="card-body">

                        <!-- Modal -->
                        {{$identitas}}
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="addModalLabel">Form Data Diri Anda</h5>
                                
                            </div>
                            <div class="modal-body">
                            <form action="/identitas/update/{{$identitas->id}}" method="POST">
                            {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label for="inputnik">NIK</label>
                                    <input type="text" class="form-control" name="nik" value="{{$identitas->nik}}" id="inputnik" placeholder="NIK">
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label for="inputnama">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="nama" value="{{$identitas->nama}}" id="inputnama" placeholder="Nama Lengkap">
                                    </div>
                                </div>
                                <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="jk">Jenis Kelamin</label>
                                @if($identitas->jk == 'L')
                                    <div class="custom-control jk-radio">
                                        <input type="radio" id="jk-radio1" name="jk" value="L" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="jk-radio1">Laki-Laki</label>
                                    </div>
                                    <div class="custom-control jk-radio">
                                        <input type="radio" id="jk-radio2" name="jk" value="P" class="custom-control-input">
                                        <label class="custom-control-label" for="jk-radio2">Perempuan</label>
                                    </div>
                                @else
                                <div class="custom-control jk-radio">
                                        <input type="radio" id="jk-radio1" name="jk" value="L" class="custom-control-input" >
                                        <label class="custom-control-label" for="jk-radio1">Laki-Laki</label>
                                    </div>
                                    <div class="custom-control jk-radio">
                                        <input type="radio" id="jk-radio2" name="jk" value="P" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="jk-radio2">Perempuan</label>
                                    </div>
                                @endif
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="inputState">Status</label>
                                        <select id="inputState" name="status" class="form-control">
                                            <option selected>Pilih...</option>
                                            <option value="Belum Kawin" @if($identitas->status == 'Belum Kawin')selected @endif>Belum Kawin</option>
                                            <option value="Kawin" @if($identitas->status == 'Kawin')selected @endif>Kawin</option>
                                            <option value="Janda" @if($identitas->status == 'Janda')selected @endif>Janda</option>
                                            <option value="Duda" @if($identitas->status == 'Duda')selected @endif>Duda</option>
                                        </select>
                                </div>
                                </div>
                                <div class="form-row">
                                
                                <div class="form-group col-md-6">
                                    <label for="inputT">Tempat Tanggal Lahir</label>
                                    <input type="text" class="form-control" name="tmpt_lahir" value="{{$identitas->ttl}}"inputT" placeholder="Tempat Tanggal Lahir">
                                </div>
                                </div>
                                <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputAddress">Alamat</label>
                                    <textarea class="form-control" id="inputAddress"  name="alamat" rows="3" placeholder="Alamat">{{$identitas->alamat}}</textarea>
                                </div>
                                <div class="form-group col-md-6">
                                        <label for="inputAgama">Agama</label>
                                        <input type="text" class="form-control" id="inputAgama" value="{{$identitas->agama}}" name="agama" placeholder="Agama">
                                </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputKerja">Pekerjaan</label>
                                        <input type="text" class="form-control" id="inputKerja" value="{{$identitas->pekerjaan}}" name="pekerjaan" placeholder="Pekerjaan">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputState">Kewarganegaraan</label>
                                        <select id="inputState" name="kewarga" class="form-control">
                                            <option selected>Pilih...</option>
                                            <option value="WNI" @if($identitas->kewarganegaraan == 'WNI') selected @endif >WNI</option>
                                            <option value="WNA" @if($identitas->kewarganegaraan == 'WNA') selected @endif >WNA</option>
                                        </select>
                                    </div>
                                </div>
                                @if($identitas->masa_berlaku == 'Berlaku Seumur Hidup')
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                            <label for="inputBerlaku">Berlaku</label>
                                            <input type="text" name="berlaku" class="form-control" value="{{$identitas->masa_berlaku}}"inputBerlaku" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-check">
                                            </br>
                                            <input class="form-check-input" value="Berlaku Seumur Hidup" onclick="myFunction()" name="berlaku" value="Seumur Hidup" type="checkbox" id="gridCheck" checked>
                                            <label class="form-check-label" for="gridCheck">
                                                Berlaku Seumur Hidup
                                            </label>
                                            <small id="ket1" class="form-text text-muted">
                                                Pilih jika masa berlaku seumur hidup
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                            <label for="inputBerlaku">Berlaku</label>
                                            <input type="text" name="berlaku" class="form-control" value="{{$identitas->masa_berlaku}}"inputBerlaku">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-check">
                                            </br>
                                            <input class="form-check-input" value="Berlaku Seumur Hidup" onclick="myFunction()" name="berlaku" value="Seumur Hidup" type="checkbox" id="gridCheck" >
                                            <label class="form-check-label" for="gridCheck">
                                                Berlaku Seumur Hidup
                                            </label>
                                            <small id="ket1" class="form-text text-muted">
                                                Pilih jika masa berlaku seumur hidup
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                @endif
                                
                            <div class="modal-footer">
                                
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                            </div>
                    </div>
                        
                    <br/>
                    <br/>
                    
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
        <script>
        function myFunction() {
        var checkBox = document.getElementById("gridCheck");
        var text = document.getElementById("inputBerlaku");
        if (checkBox.checked == true){
            $('#inputBerlaku').prop('disabled', true);
            $('#gridCheck').text($(this).attr('value'));
        } else {
            $('#inputBerlaku').prop('disabled', false);
        }
        }
        </script>
</body>
